# Salesforce Partner

Vendor: Salesforce
Homepage: https://www.salesforce.com/

Product: Salesforce Partner WSDL
Product Page: https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_partner.htm

## Introduction
We classify Salesforce Partner into the ITSM (Service Management) domain as Salesforce Partner provides ordering solutions.

"Unite marketing, sales, and service in a single app." 

## Why Integrate
The Salesforce Partner adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Salesforce Partner. With this adapter you have the ability to perform operations such as:

- Create & Update Change Request
- Create & Update Incident Ticket
- Update Change Request state
- Create Issue

## Additional Product Documentation
The [Authentication for Salesforce Partner](https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_concepts_security.htm)
