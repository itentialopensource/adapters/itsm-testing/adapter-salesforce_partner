/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-salesforce_partner',
      type: 'SalesforcePartner',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const SalesforcePartner = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Salesforce_partner Adapter Test', () => {
  describe('SalesforcePartner Class Tests', () => {
    const a = new SalesforcePartner(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-salesforce_partner-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-salesforce_partner-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const soapBindingPostDescribeNounsBodyParam = {
      describeNouns: {}
    };
    describe('#postDescribeNouns - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeNouns(soapBindingPostDescribeNounsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeNounsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SoapBinding', 'postDescribeNouns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const soapBindingPostDescribeVisualForceBodyParam = {
      describeVisualForce: {}
    };
    describe('#postDescribeVisualForce - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeVisualForce(soapBindingPostDescribeVisualForceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeVisualForceResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SoapBinding', 'postDescribeVisualForce', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const soapBindingPostGetServerTimestampBodyParam = {
      getServerTimestamp: {}
    };
    describe('#postGetServerTimestamp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGetServerTimestamp(soapBindingPostGetServerTimestampBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.getServerTimestampResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SoapBinding', 'postGetServerTimestamp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const soapBindingPostInvalidateSessionsBodyParam = {
      invalidateSessions: {}
    };
    describe('#postInvalidateSessions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postInvalidateSessions(soapBindingPostInvalidateSessionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.invalidateSessionsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SoapBinding', 'postInvalidateSessions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const soapBindingPostLoginBodyParam = {
      login: {}
    };
    describe('#postLogin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postLogin(soapBindingPostLoginBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.loginResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SoapBinding', 'postLogin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const soapBindingPostLogoutBodyParam = {
      logout: {}
    };
    describe('#postLogout - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postLogout(soapBindingPostLogoutBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.logoutResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SoapBinding', 'postLogout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostCreateBodyParam = {
      create: {}
    };
    describe('#postCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postCreate(sObjectPostCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.createResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostDeleteBodyParam = {
      delete: {}
    };
    describe('#postDelete - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDelete(sObjectPostDeleteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.deleteResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectDeletesObjectsByExampleBodyParam = {
      deleteByExample: {}
    };
    describe('#deletesObjectsByExample - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletesObjectsByExample(sObjectDeletesObjectsByExampleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.deleteByExampleResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'deletesObjectsByExample', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostDescribeApprovalLayoutBodyParam = {
      describeApprovalLayout: {}
    };
    describe('#postDescribeApprovalLayout - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeApprovalLayout(sObjectPostDescribeApprovalLayoutBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeApprovalLayoutResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postDescribeApprovalLayout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostDescribeCompactLayoutsBodyParam = {
      describeCompactLayouts: {}
    };
    describe('#postDescribeCompactLayouts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeCompactLayouts(sObjectPostDescribeCompactLayoutsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeCompactLayoutsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postDescribeCompactLayouts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostDescribeLayoutBodyParam = {
      describeLayout: {}
    };
    describe('#postDescribeLayout - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeLayout(sObjectPostDescribeLayoutBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeLayoutResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postDescribeLayout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostDescribePathAssistantsBodyParam = {
      describePathAssistants: {}
    };
    describe('#postDescribePathAssistants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribePathAssistants(sObjectPostDescribePathAssistantsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describePathAssistantsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postDescribePathAssistants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostDescribePrimaryCompactLayoutsBodyParam = {
      describePrimaryCompactLayouts: {}
    };
    describe('#postDescribePrimaryCompactLayouts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribePrimaryCompactLayouts(sObjectPostDescribePrimaryCompactLayoutsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describePrimaryCompactLayoutsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postDescribePrimaryCompactLayouts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostDescribeSObjectBodyParam = {
      describeSObject: {}
    };
    describe('#postDescribeSObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeSObject(sObjectPostDescribeSObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeSObjectResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postDescribeSObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostDescribeSObjectListViewsBodyParam = {
      describeSObjectListViews: {}
    };
    describe('#postDescribeSObjectListViews - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeSObjectListViews(sObjectPostDescribeSObjectListViewsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeSObjectListViewsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postDescribeSObjectListViews', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostDescribeSObjectsBodyParam = {
      describeSObjects: {}
    };
    describe('#postDescribeSObjects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeSObjects(sObjectPostDescribeSObjectsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeSObjectsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postDescribeSObjects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostDescribeSearchLayoutsBodyParam = {
      describeSearchLayouts: {}
    };
    describe('#postDescribeSearchLayouts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeSearchLayouts(sObjectPostDescribeSearchLayoutsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeSearchLayoutsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postDescribeSearchLayouts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostEmptyRecycleBinBodyParam = {
      emptyRecycleBin: {}
    };
    describe('#postEmptyRecycleBin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEmptyRecycleBin(sObjectPostEmptyRecycleBinBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.emptyRecycleBinResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postEmptyRecycleBin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostFindDuplicatesBodyParam = {
      findDuplicates: {}
    };
    describe('#postFindDuplicates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postFindDuplicates(sObjectPostFindDuplicatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.findDuplicatesResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postFindDuplicates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectFindDuplicatessObjectsByIdsBodyParam = {
      findDuplicatesByIds: {}
    };
    describe('#findDuplicatessObjectsByIds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findDuplicatessObjectsByIds(sObjectFindDuplicatessObjectsByIdsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.findDuplicatesByIdsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'findDuplicatessObjectsByIds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostGetDeletedBodyParam = {
      getDeleted: {}
    };
    describe('#postGetDeleted - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGetDeleted(sObjectPostGetDeletedBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.getDeletedResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postGetDeleted', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostGetUpdatedBodyParam = {
      getUpdated: {}
    };
    describe('#postGetUpdated - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGetUpdated(sObjectPostGetUpdatedBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.getUpdatedResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postGetUpdated', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostMergeBodyParam = {
      merge: {}
    };
    describe('#postMerge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMerge(sObjectPostMergeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.mergeResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postMerge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostRetrieveBodyParam = {
      retrieve: {}
    };
    describe('#postRetrieve - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRetrieve(sObjectPostRetrieveBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.retrieveResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postRetrieve', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostSearchBodyParam = {
      search: {}
    };
    describe('#postSearch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSearch(sObjectPostSearchBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.searchResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postSearch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostUndeleteBodyParam = {
      undelete: {}
    };
    describe('#postUndelete - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUndelete(sObjectPostUndeleteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.undeleteResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postUndelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostUpdateBodyParam = {
      update: {}
    };
    describe('#postUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUpdate(sObjectPostUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.updateResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostUpsertBodyParam = {
      upsert: {}
    };
    describe('#postUpsert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUpsert(sObjectPostUpsertBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.upsertResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postUpsert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const globalPostDescribeGlobalBodyParam = {
      describeGlobal: {}
    };
    describe('#postDescribeGlobal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeGlobal(globalPostDescribeGlobalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeGlobalResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Global', 'postDescribeGlobal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataCategoryPostDescribeDataCategoryGroupStructuresBodyParam = {
      describeDataCategoryGroupStructures: {}
    };
    describe('#postDescribeDataCategoryGroupStructures - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postDescribeDataCategoryGroupStructures(dataCategoryPostDescribeDataCategoryGroupStructuresBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-salesforce_partner-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataCategory', 'postDescribeDataCategoryGroupStructures', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataCategoryPostDescribeDataCategoryGroupsBodyParam = {
      describeDataCategoryGroups: {}
    };
    describe('#postDescribeDataCategoryGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeDataCategoryGroups(dataCategoryPostDescribeDataCategoryGroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeDataCategoryGroupsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataCategory', 'postDescribeDataCategoryGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataCategoryPostDescribeDataCategoryMappingsBodyParam = {
      describeDataCategoryMappings: {}
    };
    describe('#postDescribeDataCategoryMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeDataCategoryMappings(dataCategoryPostDescribeDataCategoryMappingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeDataCategoryMappingsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataCategory', 'postDescribeDataCategoryMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userInfoPostChangeOwnPasswordBodyParam = {
      changeOwnPassword: {}
    };
    describe('#postChangeOwnPassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postChangeOwnPassword(userInfoPostChangeOwnPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.changeOwnPasswordResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserInfo', 'postChangeOwnPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userInfoPostDescribeAllTabsBodyParam = {
      describeAllTabs: {}
    };
    describe('#postDescribeAllTabs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeAllTabs(userInfoPostDescribeAllTabsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeAllTabsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserInfo', 'postDescribeAllTabs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userInfoPostDescribeKnowledgeSettingsBodyParam = {
      describeKnowledgeSettings: {}
    };
    describe('#postDescribeKnowledgeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeKnowledgeSettings(userInfoPostDescribeKnowledgeSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeKnowledgeSettingsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserInfo', 'postDescribeKnowledgeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userInfoPostDescribeSearchScopeOrderBodyParam = {
      describeSearchScopeOrder: {}
    };
    describe('#postDescribeSearchScopeOrder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeSearchScopeOrder(userInfoPostDescribeSearchScopeOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeSearchScopeOrderResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserInfo', 'postDescribeSearchScopeOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userInfoPostDescribeSearchableEntitiesBodyParam = {
      describeSearchableEntities: {}
    };
    describe('#postDescribeSearchableEntities - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeSearchableEntities(userInfoPostDescribeSearchableEntitiesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeSearchableEntitiesResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserInfo', 'postDescribeSearchableEntities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userInfoPostDescribeSoqlListViewsBodyParam = {
      describeSoqlListViews: {}
    };
    describe('#postDescribeSoqlListViews - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeSoqlListViews(userInfoPostDescribeSoqlListViewsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeSoqlListViewsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserInfo', 'postDescribeSoqlListViews', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userInfoPostDescribeTabsBodyParam = {
      describeTabs: {}
    };
    describe('#postDescribeTabs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeTabs(userInfoPostDescribeTabsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeTabsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserInfo', 'postDescribeTabs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userInfoPostExecuteListViewBodyParam = {
      executeListView: {}
    };
    describe('#postExecuteListView - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postExecuteListView(userInfoPostExecuteListViewBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.executeListViewResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserInfo', 'postExecuteListView', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userInfoPostGetUserInfoBodyParam = {
      getUserInfo: {}
    };
    describe('#postGetUserInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGetUserInfo(userInfoPostGetUserInfoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.getUserInfoResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserInfo', 'postGetUserInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userInfoPostResetPasswordBodyParam = {
      resetPassword: {}
    };
    describe('#postResetPassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postResetPassword(userInfoPostResetPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.resetPasswordResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserInfo', 'postResetPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userInfoPostSetPasswordBodyParam = {
      setPassword: {}
    };
    describe('#postSetPassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSetPassword(userInfoPostSetPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.setPasswordResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserInfo', 'postSetPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appPostDescribeAppMenuBodyParam = {
      describeAppMenu: {}
    };
    describe('#postDescribeAppMenu - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeAppMenu(appPostDescribeAppMenuBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeAppMenuResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'postDescribeAppMenu', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const themePostDescribeGlobalThemeBodyParam = {
      describeGlobalTheme: {}
    };
    describe('#postDescribeGlobalTheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeGlobalTheme(themePostDescribeGlobalThemeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeGlobalThemeResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Theme', 'postDescribeGlobalTheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const themePostDescribeThemeBodyParam = {
      describeTheme: {}
    };
    describe('#postDescribeTheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeTheme(themePostDescribeThemeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeThemeResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Theme', 'postDescribeTheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const softPhonePostDescribeSoftphoneLayoutBodyParam = {
      describeSoftphoneLayout: {}
    };
    describe('#postDescribeSoftphoneLayout - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeSoftphoneLayout(softPhonePostDescribeSoftphoneLayoutBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeSoftphoneLayoutResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SoftPhone', 'postDescribeSoftphoneLayout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowPostProcessBodyParam = {
      process: {}
    };
    describe('#postProcess - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postProcess(workflowPostProcessBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.processResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflow', 'postProcess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const leadPostConvertLeadBodyParam = {
      convertLead: {}
    };
    describe('#postConvertLead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postConvertLead(leadPostConvertLeadBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.convertLeadResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Lead', 'postConvertLead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const queryPostQueryBodyParam = {
      query: {}
    };
    describe('#postQuery - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postQuery(queryPostQueryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.queryResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Query', 'postQuery', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const queryPostQueryAllBodyParam = {
      queryAll: {}
    };
    describe('#postQueryAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postQueryAll(queryPostQueryAllBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.queryAllResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Query', 'postQueryAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const queryPostQueryMoreBodyParam = {
      queryMore: {}
    };
    describe('#postQueryMore - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postQueryMore(queryPostQueryMoreBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.queryMoreResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Query', 'postQueryMore', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const emailPostRenderEmailTemplateBodyParam = {
      renderEmailTemplate: {}
    };
    describe('#postRenderEmailTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRenderEmailTemplate(emailPostRenderEmailTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.renderEmailTemplateResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Email', 'postRenderEmailTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const emailPostRenderStoredEmailTemplateBodyParam = {
      renderStoredEmailTemplate: {}
    };
    describe('#postRenderStoredEmailTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRenderStoredEmailTemplate(emailPostRenderStoredEmailTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.renderStoredEmailTemplateResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Email', 'postRenderStoredEmailTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const emailPostSendEmailBodyParam = {
      sendEmail: {}
    };
    describe('#postSendEmail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSendEmail(emailPostSendEmailBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.sendEmailResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Email', 'postSendEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const emailPostSendEmailMessageBodyParam = {
      sendEmailMessage: {}
    };
    describe('#postSendEmailMessage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSendEmailMessage(emailPostSendEmailMessageBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.sendEmailMessageResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Email', 'postSendEmailMessage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const quickActionPostDescribeAvailableQuickActionsBodyParam = {
      describeAvailableQuickActions: {}
    };
    describe('#postDescribeAvailableQuickActions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeAvailableQuickActions(quickActionPostDescribeAvailableQuickActionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeAvailableQuickActionsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('QuickAction', 'postDescribeAvailableQuickActions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const quickActionPostDescribeQuickActionsBodyParam = {
      describeQuickActions: {}
    };
    describe('#postDescribeQuickActions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeQuickActions(quickActionPostDescribeQuickActionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeQuickActionsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('QuickAction', 'postDescribeQuickActions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const quickActionPostDescribeQuickActionsForRecordTypeBodyParam = {
      describeQuickActionsForRecordType: {}
    };
    describe('#postDescribeQuickActionsForRecordType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeQuickActionsForRecordType(quickActionPostDescribeQuickActionsForRecordTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeQuickActionsForRecordTypeResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('QuickAction', 'postDescribeQuickActionsForRecordType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const quickActionPostPerformQuickActionsBodyParam = {
      performQuickActions: {}
    };
    describe('#postPerformQuickActions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postPerformQuickActions(quickActionPostPerformQuickActionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.performQuickActionsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('QuickAction', 'postPerformQuickActions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const quickActionPostRetrieveMassQuickActionTemplatesBodyParam = {
      retrieveMassQuickActionTemplates: {}
    };
    describe('#postRetrieveMassQuickActionTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRetrieveMassQuickActionTemplates(quickActionPostRetrieveMassQuickActionTemplatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.retrieveMassQuickActionTemplatesResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('QuickAction', 'postRetrieveMassQuickActionTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const quickActionPostRetrieveQuickActionTemplatesBodyParam = {
      retrieveQuickActionTemplates: {}
    };
    describe('#postRetrieveQuickActionTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRetrieveQuickActionTemplates(quickActionPostRetrieveQuickActionTemplatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.retrieveQuickActionTemplatesResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('QuickAction', 'postRetrieveQuickActionTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
