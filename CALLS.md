## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Salesforce Partner. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Salesforce Partner.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Salesforce Partner. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">postLogin(body, callback)</td>
    <td style="padding:15px">login</td>
    <td style="padding:15px">{base_path}/{version}/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogout(body, callback)</td>
    <td style="padding:15px">logout</td>
    <td style="padding:15px">{base_path}/{version}/logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvalidateSessions(body, callback)</td>
    <td style="padding:15px">invalidateSessions</td>
    <td style="padding:15px">{base_path}/{version}/invalidateSessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetServerTimestamp(body, callback)</td>
    <td style="padding:15px">getServerTimestamp</td>
    <td style="padding:15px">{base_path}/{version}/getServerTimestamp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeVisualForce(body, callback)</td>
    <td style="padding:15px">describeVisualForce</td>
    <td style="padding:15px">{base_path}/{version}/describeVisualForce?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeNouns(body, callback)</td>
    <td style="padding:15px">describeNouns</td>
    <td style="padding:15px">{base_path}/{version}/describeNouns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeSObject(body, callback)</td>
    <td style="padding:15px">describeSObject</td>
    <td style="padding:15px">{base_path}/{version}/describeSObject?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeSObjects(body, callback)</td>
    <td style="padding:15px">describeSObjects</td>
    <td style="padding:15px">{base_path}/{version}/describeSObjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeLayout(body, callback)</td>
    <td style="padding:15px">describeLayout</td>
    <td style="padding:15px">{base_path}/{version}/describeLayout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeSearchLayouts(body, callback)</td>
    <td style="padding:15px">describeSearchLayouts</td>
    <td style="padding:15px">{base_path}/{version}/describeSearchLayouts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeCompactLayouts(body, callback)</td>
    <td style="padding:15px">describeCompactLayouts</td>
    <td style="padding:15px">{base_path}/{version}/describeCompactLayouts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribePathAssistants(body, callback)</td>
    <td style="padding:15px">describePathAssistants</td>
    <td style="padding:15px">{base_path}/{version}/describePathAssistants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeApprovalLayout(body, callback)</td>
    <td style="padding:15px">describeApprovalLayout</td>
    <td style="padding:15px">{base_path}/{version}/describeApprovalLayout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeSObjectListViews(body, callback)</td>
    <td style="padding:15px">describeSObjectListViews</td>
    <td style="padding:15px">{base_path}/{version}/describeSObjectListViews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribePrimaryCompactLayouts(body, callback)</td>
    <td style="padding:15px">describePrimaryCompactLayouts</td>
    <td style="padding:15px">{base_path}/{version}/describePrimaryCompactLayouts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreate(body, callback)</td>
    <td style="padding:15px">create</td>
    <td style="padding:15px">{base_path}/{version}/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdate(body, callback)</td>
    <td style="padding:15px">update</td>
    <td style="padding:15px">{base_path}/{version}/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpsert(body, callback)</td>
    <td style="padding:15px">upsert</td>
    <td style="padding:15px">{base_path}/{version}/upsert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMerge(body, callback)</td>
    <td style="padding:15px">merge</td>
    <td style="padding:15px">{base_path}/{version}/merge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDelete(body, callback)</td>
    <td style="padding:15px">delete</td>
    <td style="padding:15px">{base_path}/{version}/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUndelete(body, callback)</td>
    <td style="padding:15px">undelete</td>
    <td style="padding:15px">{base_path}/{version}/undelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEmptyRecycleBin(body, callback)</td>
    <td style="padding:15px">emptyRecycleBin</td>
    <td style="padding:15px">{base_path}/{version}/emptyRecycleBin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRetrieve(body, callback)</td>
    <td style="padding:15px">retrieve</td>
    <td style="padding:15px">{base_path}/{version}/retrieve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDeleted(body, callback)</td>
    <td style="padding:15px">getDeleted</td>
    <td style="padding:15px">{base_path}/{version}/getDeleted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetUpdated(body, callback)</td>
    <td style="padding:15px">getUpdated</td>
    <td style="padding:15px">{base_path}/{version}/getUpdated?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSearch(body, callback)</td>
    <td style="padding:15px">search</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletesObjectsByExample(body, callback)</td>
    <td style="padding:15px">deleteByExample</td>
    <td style="padding:15px">{base_path}/{version}/deleteByExample?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFindDuplicates(body, callback)</td>
    <td style="padding:15px">findDuplicates</td>
    <td style="padding:15px">{base_path}/{version}/findDuplicates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findDuplicatessObjectsByIds(body, callback)</td>
    <td style="padding:15px">findDuplicatesByIds</td>
    <td style="padding:15px">{base_path}/{version}/findDuplicatesByIds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeGlobal(body, callback)</td>
    <td style="padding:15px">describeGlobal</td>
    <td style="padding:15px">{base_path}/{version}/describeGlobal?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeDataCategoryGroups(body, callback)</td>
    <td style="padding:15px">describeDataCategoryGroups</td>
    <td style="padding:15px">{base_path}/{version}/describeDataCategoryGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeDataCategoryGroupStructures(body, callback)</td>
    <td style="padding:15px">describeDataCategoryGroupStructures</td>
    <td style="padding:15px">{base_path}/{version}/describeDataCategoryGroupStructures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeDataCategoryMappings(body, callback)</td>
    <td style="padding:15px">describeDataCategoryMappings</td>
    <td style="padding:15px">{base_path}/{version}/describeDataCategoryMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeKnowledgeSettings(body, callback)</td>
    <td style="padding:15px">describeKnowledgeSettings</td>
    <td style="padding:15px">{base_path}/{version}/describeKnowledgeSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeSearchableEntities(body, callback)</td>
    <td style="padding:15px">describeSearchableEntities</td>
    <td style="padding:15px">{base_path}/{version}/describeSearchableEntities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeSearchScopeOrder(body, callback)</td>
    <td style="padding:15px">describeSearchScopeOrder</td>
    <td style="padding:15px">{base_path}/{version}/describeSearchScopeOrder?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeSoqlListViews(body, callback)</td>
    <td style="padding:15px">describeSoqlListViews</td>
    <td style="padding:15px">{base_path}/{version}/describeSoqlListViews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExecuteListView(body, callback)</td>
    <td style="padding:15px">executeListView</td>
    <td style="padding:15px">{base_path}/{version}/executeListView?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeTabs(body, callback)</td>
    <td style="padding:15px">describeTabs</td>
    <td style="padding:15px">{base_path}/{version}/describeTabs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeAllTabs(body, callback)</td>
    <td style="padding:15px">describeAllTabs</td>
    <td style="padding:15px">{base_path}/{version}/describeAllTabs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSetPassword(body, callback)</td>
    <td style="padding:15px">setPassword</td>
    <td style="padding:15px">{base_path}/{version}/setPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChangeOwnPassword(body, callback)</td>
    <td style="padding:15px">changeOwnPassword</td>
    <td style="padding:15px">{base_path}/{version}/changeOwnPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetPassword(body, callback)</td>
    <td style="padding:15px">resetPassword</td>
    <td style="padding:15px">{base_path}/{version}/resetPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetUserInfo(body, callback)</td>
    <td style="padding:15px">getUserInfo</td>
    <td style="padding:15px">{base_path}/{version}/getUserInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeAppMenu(body, callback)</td>
    <td style="padding:15px">describeAppMenu</td>
    <td style="padding:15px">{base_path}/{version}/describeAppMenu?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeGlobalTheme(body, callback)</td>
    <td style="padding:15px">describeGlobalTheme</td>
    <td style="padding:15px">{base_path}/{version}/describeGlobalTheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeTheme(body, callback)</td>
    <td style="padding:15px">describeTheme</td>
    <td style="padding:15px">{base_path}/{version}/describeTheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeSoftphoneLayout(body, callback)</td>
    <td style="padding:15px">describeSoftphoneLayout</td>
    <td style="padding:15px">{base_path}/{version}/describeSoftphoneLayout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProcess(body, callback)</td>
    <td style="padding:15px">process</td>
    <td style="padding:15px">{base_path}/{version}/process?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConvertLead(body, callback)</td>
    <td style="padding:15px">convertLead</td>
    <td style="padding:15px">{base_path}/{version}/convertLead?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postQuery(body, callback)</td>
    <td style="padding:15px">query</td>
    <td style="padding:15px">{base_path}/{version}/query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postQueryAll(body, callback)</td>
    <td style="padding:15px">queryAll</td>
    <td style="padding:15px">{base_path}/{version}/queryAll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postQueryMore(body, callback)</td>
    <td style="padding:15px">queryMore</td>
    <td style="padding:15px">{base_path}/{version}/queryMore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSendEmailMessage(body, callback)</td>
    <td style="padding:15px">sendEmailMessage</td>
    <td style="padding:15px">{base_path}/{version}/sendEmailMessage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSendEmail(body, callback)</td>
    <td style="padding:15px">sendEmail</td>
    <td style="padding:15px">{base_path}/{version}/sendEmail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRenderEmailTemplate(body, callback)</td>
    <td style="padding:15px">renderEmailTemplate</td>
    <td style="padding:15px">{base_path}/{version}/renderEmailTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRenderStoredEmailTemplate(body, callback)</td>
    <td style="padding:15px">renderStoredEmailTemplate</td>
    <td style="padding:15px">{base_path}/{version}/renderStoredEmailTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPerformQuickActions(body, callback)</td>
    <td style="padding:15px">performQuickActions</td>
    <td style="padding:15px">{base_path}/{version}/performQuickActions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeQuickActions(body, callback)</td>
    <td style="padding:15px">describeQuickActions</td>
    <td style="padding:15px">{base_path}/{version}/describeQuickActions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeQuickActionsForRecordType(body, callback)</td>
    <td style="padding:15px">describeQuickActionsForRecordType</td>
    <td style="padding:15px">{base_path}/{version}/describeQuickActionsForRecordType?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDescribeAvailableQuickActions(body, callback)</td>
    <td style="padding:15px">describeAvailableQuickActions</td>
    <td style="padding:15px">{base_path}/{version}/describeAvailableQuickActions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRetrieveQuickActionTemplates(body, callback)</td>
    <td style="padding:15px">retrieveQuickActionTemplates</td>
    <td style="padding:15px">{base_path}/{version}/retrieveQuickActionTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRetrieveMassQuickActionTemplates(body, callback)</td>
    <td style="padding:15px">retrieveMassQuickActionTemplates</td>
    <td style="padding:15px">{base_path}/{version}/retrieveMassQuickActionTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
