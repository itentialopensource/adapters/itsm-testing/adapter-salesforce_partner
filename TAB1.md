# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Salesforce_partner System. The API that was used to build the adapter for Salesforce_partner is usually available in the report directory of this adapter. The adapter utilizes the Salesforce_partner API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Salesforce Partner adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Salesforce Partner. With this adapter you have the ability to perform operations such as:

- Create & Update Change Request
- Create & Update Incident Ticket
- Update Change Request state
- Create Issue

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
